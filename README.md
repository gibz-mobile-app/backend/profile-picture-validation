# Projekt DEAD
In diesem Projekt haben wir ein eine .Net Rest Api implementiert. Diese Api besteht aus einem einzigen Endpunkt, welcher ein Bild erhält und dieses prozessiert. Dieser Prozess besteht daraus, dass wir das zugeschickte Bild validieren, bearbeiten und dann an einen Profile Picture Server weiterschicken.

## Architekturdiagramm
![Architekturdiagramm](./doc/Architekturdiagramm_DEAD.jpg)

## Sequenzdiagramm
![Sequenzdiagramm](./doc/sequencediagram.png)

## Ablaufdiagramm
![Ablaufdiagramm](./doc/Ablaufdiagramm.png)

## Designentscheide
Wir haben uns entschieden einen eigenen Service zu bauen. Der Grund dafür ist, dass wir dadurch die existierenden Programme nicht viel verändern müssen und man unser Projekt, wenn nötig, leicht ersetzen kann. Unsere Lösung ist ja, dass wir unseren Service zwischen die Mobile App und den Profile Picture Server "quetschen", das bedeutet, wir müssen nur das Routing verändern und sonst nichts, ausserhalb unseres Projektes. Die Mobile App schickt uns das Bild, wir führen unseren Prozess durch und schicken es dann, wenn alles gut gelaufen ist, an den Profile Picture Server weiter. Das wäre so nicht möglich gewesen, ohne einen eigenen Service zu erstellen.
Um dieses Projekt durchzuführenn haben wir eine .Net Rest Api erstellt, weil wir alle schon Erfahrungen mit dieser Technologie haben.


## Systemkomponenten
Unsere Kriterien für die Wahl unsere Komponenten waren:
- Sie sollten nicht zu kompliziert sein, proportional zu der Komplexität ihrer Aufgabe
- Genügende Dokumentation, damit unsere Arbeit nicht zu schwierig wird
- Muss kompatibel sein mit C#

Unsere Anforderungen für das Validieren eines Bildes waren:
- Erkennen, dass es nur ein Gesicht hat
- Erkennen, ob das Gesicht gerade aus schaut
- Erkennen, ob das Gesicht bedeckt ist

Für diese Anforderungen haben wir [Google Vision Api](https://cloud.google.com/vision?hl=de) gebraucht, weil wir damit direkt alle diese Anforderungen abchecken können. Die Dokumentation war gut erklärt in C# und die Api ist weitverbreitet. 

Zusätzlich sollten wir den Hintergrund entfernen und das Bild um das Gesicht zuschneiden. Für die Entfernung des Hintergrundes, haben wir die [Remove bg Api](https://www.remove.bg/api) benutzt. Diese Api war eine der Ersten, welche vorgeschlagen wurden. Mit der Dokumentation war es einfach das Ganze auszuprobieren und anwenden. 

Für das Croppen des Bildes verwenden wir zuerst Google Vision Api um die Koordianaten des Gesichtes zu bekommen. Anhand diesen Koordianaten können wir nun angeben wie viel Abstand diese Koordianaten zum Bildrand haben sollen. An dieser stelle croppen wir das Bild mit der [ImageSharp Api](https://sixlabors.com/products/imagesharp/) das Bild. Für die ImageSharp Api haben wir und entschieden, da sie für uns em einfachsten zu gebrauchen war. Google Vision Api verwenden wir hier für das Erkennen vom Gesicht, weil wir genau das gleich auch schon für die Face Detection benutzt haben. Somit war es ziemlich einfach, das gleiche hier zu implementieren.

## Anleitung für Postman
1. Öffnen Sie Postman: Erstellen Sie einen neuen Request
2. Setzen Sie die Request Methode auf "POST"
3. Geben Sie diese URL ein: "https://localhost:7173/v1/images/Upload"
4. Gehen Sie zu Body: Wählen Sie die Form-Data im Body aus
5. Fügen Sie die Key-Value Pairs zu:
* Key: pfp, Type: File - Wählen Sie ein Bild aus
* Key: student_id, Type: Text - Schreiben Sie irgend eine Id ein
6. Senden Sie den Request ab
