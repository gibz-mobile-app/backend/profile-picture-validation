namespace ProfilePictureValidation.Utilities;

public class RemoveBgConfiguration
{
    public string ApiKey { get; set; } = null!;
    public string ServiceBaseUrl { get; set; } = null!;
    public string BackgroundRemovalEndpointPath { get; set; } = null!;
    public string AccountInfoEndpointPath { get; set; } = null!;
    public string OutputSize { get; set; } = null!;
    public string ForegroundType { get; set; } = null!;
    public string BackgroundImageUrl { get; set; } = null!;
    public int RemainingCreditsThreshold { get; set; }

}
