namespace ProfilePictureValidation.Utilities;

public class FaceOrientationConfiguration
{
    public int MaxPanAngle { get; set; }
    public int MaxRollAngle { get; set; }
    public int MaxTiltAngle { get; set; }
    public float RollAngleThreshold { get; set; }
}