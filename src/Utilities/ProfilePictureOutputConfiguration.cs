namespace ProfilePictureValidation.Utilities;

public class ProfilePictureOutputConfiguration
{
    public int Height { get; set; }
    public int Width { get; set; }
    public float FaceHeightRatio { get; set; }
    public float EyeLineHeight { get; set; }
    public string OutputUrl { get; set; } = null!;
    public bool PersistOriginalImage { get; set; }
}