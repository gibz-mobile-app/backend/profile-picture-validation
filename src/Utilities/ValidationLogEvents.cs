namespace ProfilePictureValidation.Utilities;

public class ValidationLogEvents
{
    public const int DeliveryStartet = 1001;
    public const int PersistedOriginalImage = 1002;

    public const int InvalidFileDelivered = 2001;
    public const int MissingTokenInValidationRequest = 2002;

    public const int AbnormalFaceCount = 3001;
    public const int FaceOrientationOutOfBounds = 3002;
    public const int InsufficientFaceDimensions = 3003;
    public const int DetectedFaceLandmark = 3004;
    public const int MissingFaceLandmark = 3005;

    public const int FixedFaceRoll = 4001;
    public const int FaceTooSmallForCropping = 4002;
    public const int AdjustDimensions = 4003;
    public const int FixAspectRatio = 4004;
    public const int ExtendImage = 4005;

    public const int BackgroundRemoval = 5001;
    public const int RemainingCreditsReporting = 5002;

    public const int RemotePicturePersistenceSuccessful = 6001;
    public const int RemotePicturePersistenceFailed = 6002;
}
