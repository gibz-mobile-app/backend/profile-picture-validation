namespace ProfilePictureValidation.Models;

public enum EFaceLandmark
{
    LEFT_EYE,
    RIGHT_EYE,
    NOSE_TIP,
    MIDPOINT_BETWEEN_EYES,
}