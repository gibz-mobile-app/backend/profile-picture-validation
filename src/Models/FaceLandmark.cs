namespace ProfilePictureValidation.Models;

public record FaceLandmark(EFaceLandmark Type, float X, float Y);

public record ValidationFlag(string Label, float? Value);