namespace ProfilePictureValidation.Models;

public class FaceInformation
{
    public float PanAngle { get; init; }
    public float RollAngle { get; init; }
    public float TiltAngle { get; init; }

    public int TopOfHead { get; set; }
    public int BottomOfHead { get; set; }
    public int FaceHeight => BottomOfHead - TopOfHead;

    public ELikelihood UnderExposedLikelihood { get; init; }
    public ELikelihood BlurredLikelihood { get; init; }
    public ELikelihood HeadwearLikelihood { get; init; }

    public List<FaceLandmark> Landmarks { get; } = [];
    public int EyeLineHeight
    {
        get
        {
            var leftEye = Landmarks.First(landmark => landmark.Type == EFaceLandmark.LEFT_EYE);
            var rightEye = Landmarks.First(landmark => landmark.Type == EFaceLandmark.RIGHT_EYE);

            return (int)(leftEye.Y + rightEye.Y) / 2;
        }
    }
}