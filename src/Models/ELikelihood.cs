namespace ProfilePictureValidation.Models;

public enum ELikelihood
{
    UNKNOWN,
    VERY_UNLIKELY,
    UNLIKELY,
    POSSIBLE,
    LIKELY,
    VERY_LIKELY
}