using Google.Cloud.Diagnostics.AspNetCore3;
using Google.Cloud.Diagnostics.Common;
using ProfilePictureValidation.Services;
using ProfilePictureValidation.Utilities;

var builder = WebApplication.CreateBuilder(args);

// Configure logging: Add Google Cloud Logging as log provider
builder.Services.AddGoogleDiagnosticsForAspNetCore("gibz-app", loggingOptions: LoggingOptions.Create(logLevel: LogLevel.Trace));

// Add controllers
builder.Services.AddControllers();

// Add configuration
builder.Services.Configure<FaceOrientationConfiguration>(builder.Configuration.GetSection("ProfilePictureValidation:FaceOrientation"));
builder.Services.Configure<RemoveBgConfiguration>(builder.Configuration.GetSection("BackgroundRemoval:RemoveBg"));
builder.Services.Configure<ProfilePictureOutputConfiguration>(builder.Configuration.GetSection("ProfilePictureOutput"));

// Add services
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddTransient<ICroppingService, CroppingService>();
builder.Services.AddTransient<IBackgroundRemovalService, RemoveBgBackgroundRemovalService>();
builder.Services.AddScoped<IFaceValidationService, GoogleFaceValidationService>();
builder.Services.AddScoped<IValidationFlagService, InMemoryValidationFlagService>();
builder.Services.AddScoped<IPicturePersistenceService, RemotePicturePersistenceService>();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();