using Microsoft.AspNetCore.Mvc;

namespace ProfilePictureValidation.Controllers;

[ApiController]
[Route("[controller]")]
public class HealthController : ControllerBase
{
    [HttpGet("readiness")]
    public IActionResult CheckReadiness()
    {
        return Ok();
    }
}