﻿using Microsoft.AspNetCore.Mvc;
using ProfilePictureValidation.Services;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp;
using Image = SixLabors.ImageSharp.Image;
using SixLabors.ImageSharp.Processing;
using ProfilePictureValidation.Exceptions;
using Microsoft.Extensions.Options;
using ProfilePictureValidation.Utilities;

namespace ProfilePictureValidation.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public class ProfilePictureController(
    IFaceValidationService _faceDetectionService,
    IBackgroundRemovalService _backgroundRemovalService,
    ICroppingService _croppingService,
    IPicturePersistenceService _persistenceService,
    IOptions<ProfilePictureOutputConfiguration> outputConfiguration,
    ILogger<ProfilePictureController> _logger
    ) : ControllerBase
{
    private readonly ProfilePictureOutputConfiguration _outputConfiguration = outputConfiguration.Value;


    [HttpPost]
    public async Task<IActionResult> ProcessImage([FromForm] IFormFile profilePicture, [FromForm] string token)
    {
        if (string.IsNullOrEmpty(token))
        {
            _logger.LogError(ValidationLogEvents.MissingTokenInValidationRequest, "Missing token in validation request.");
            return BadRequest("Token must not be empty");
        }

        try
        {
            if (profilePicture is null || profilePicture.Length == 0)
            {
                _logger.LogError(ValidationLogEvents.InvalidFileDelivered, "Missing image file for validation request.");
                return BadRequest($"You must provide a file with MIME type '{System.Net.Mime.MediaTypeNames.Image.Jpeg}' as value for the field '{nameof(profilePicture)}'.");
            }
            else if (profilePicture.ContentType != System.Net.Mime.MediaTypeNames.Image.Jpeg)
            {
                _logger.LogError(ValidationLogEvents.InvalidFileDelivered, "Delivered file with invalid MIME type: {mimeType}", profilePicture.ContentType);
                return BadRequest($"You must provide a file with MIME type '{System.Net.Mime.MediaTypeNames.Image.Jpeg}' as value for the field '{nameof(profilePicture)}'.");
            }

            using (_logger.BeginScope(new Dictionary<string, object> { { "token", token } }))
            {
                _logger.LogDebug(ValidationLogEvents.DeliveryStartet, "Image delivered for token {token}", token);

                // Prepare image: Load the image using SixLabors.ImageSharp
                using var memoryStream = new MemoryStream();
                await profilePicture.CopyToAsync(memoryStream);
                var image = Image.Load<Rgba32>(memoryStream.ToArray());

                if (_outputConfiguration.PersistOriginalImage)
                {
                    var originalImage = memoryStream.ToArray();
                    _ = _persistenceService.PersistProfilePictureAsync(token, originalImage, true);
                    _logger.LogDebug(ValidationLogEvents.PersistedOriginalImage, "Persisted original image for token {token}.", token);
                }

                // Fix orientation in case the source image is not oriented properly.
                image.Mutate(img => img.AutoOrient());
                image.SaveAsJpeg(memoryStream);
                var imageBytes = memoryStream.ToArray();

                // Validate the image
                var faceInformation = await _faceDetectionService.GetFaceInformationAsync(imageBytes);
                _faceDetectionService.ValidateFaceOrientation(faceInformation);
                _faceDetectionService.ValidateDimensions(faceInformation);

                // Crop image...
                var croppedImage = await _croppingService.CropImageAsync(image, faceInformation);

                // ... and then ASYNCHRONOUSLY remove background and send final image to the persistence service
                _ = _backgroundRemovalService.RemoveBackgroundAsync(croppedImage)
                    .ContinueWith(
                        finalImage =>
                        {
                            _persistenceService.PersistProfilePictureAsync(token, finalImage.Result);
                        }, TaskContinuationOptions.OnlyOnRanToCompletion);

                return NoContent();
            }
        }
        catch (ProfilePictureValidationException exception)
        {
            var response = new Dictionary<string, object?>{
                {"type", exception.GetType().Name},
                {"errorCode", exception.ErrorCode},
                {"message", exception.Message},
                {"details", exception.Details},
            };
            return StatusCode(400, response);
        }
        catch (Exception exception)
        {
            return StatusCode(500, exception.Message);
        }
    }
}