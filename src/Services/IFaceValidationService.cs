﻿using ProfilePictureValidation.Models;

namespace ProfilePictureValidation.Services;
public interface IFaceValidationService
{
    Task<FaceInformation> GetFaceInformationAsync(byte[] image);

    void ValidateFaceOrientation(FaceInformation faceInformation);

    void ValidateDimensions(FaceInformation faceInformation);
}