﻿using Microsoft.Extensions.Options;
using ProfilePictureValidation.Exceptions;
using ProfilePictureValidation.Models;
using ProfilePictureValidation.Utilities;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace ProfilePictureValidation.Services;
public class CroppingService(
    IOptions<ProfilePictureOutputConfiguration> outputConfiguration,
    IOptions<FaceOrientationConfiguration> faceOrientationConfiguration,
    IFaceValidationService _faceValidationService,
    ILogger<CroppingService> _logger
) : ICroppingService
{
    private readonly ProfilePictureOutputConfiguration _outputConfiguration = outputConfiguration.Value;
    private readonly FaceOrientationConfiguration _faceOrientationConfiguration = faceOrientationConfiguration.Value;

    /// <summary>
    /// Crops the passed image, for the face to be centered.
    /// </summary>
    /// <param name="image">The image that should be cropped.</param>
    /// <param name="faceInfo">Metadata for the detected face of the image.</param>
    /// <returns>A byte array, which includes the cropped image.</returns>
    public async Task<byte[]> CropImageAsync(Image<Rgba32> image, FaceInformation faceInfo)
    {
        if (Math.Abs(faceInfo.RollAngle) > _faceOrientationConfiguration.RollAngleThreshold)
        {
            // Fix the roll angle
            if (Math.Abs(faceInfo.RollAngle) <= 45)
            {
                image.Mutate(ctx => ctx.Rotate(-1 * faceInfo.RollAngle));

                _logger.LogDebug(ValidationLogEvents.FixedFaceRoll,
                                 "Automatically fixed roll angle since angle of roll rotation ({rollIs} degrees) was above threshold ({rollThreshold} degrees).",
                                 faceInfo.RollAngle,
                                 _faceOrientationConfiguration.RollAngleThreshold);
                                 
                // Redo face detection
                using var ms = new MemoryStream();
                image.SaveAsJpeg(ms);
                faceInfo = await _faceValidationService.GetFaceInformationAsync(ms.ToArray());
            }

        }

        // Calculate final aspect ratio
        var aspectRatio = _outputConfiguration.Width / (float)_outputConfiguration.Height;

        // Determine target height of face
        var targetFaceHeight = _outputConfiguration.Height * _outputConfiguration.FaceHeightRatio;

        // Discard images for which the face needs to be scaled up.
        if (targetFaceHeight / faceInfo.FaceHeight > 1)
        {
            _logger.LogWarning(ValidationLogEvents.FaceTooSmallForCropping,
                               "Face is too small for target size of profile picture (face height: {heightIs}, target height: {heightRequired}).",
                               faceInfo.FaceHeight,
                               targetFaceHeight);
            throw new CroppingSizeException();
        }

        // Calculate center of face based on detected landmarks
        var noseTip = faceInfo.Landmarks.First(landmark => landmark.Type == EFaceLandmark.MIDPOINT_BETWEEN_EYES);
        var centerX = noseTip.X;
        var centerY = faceInfo.EyeLineHeight + 0.02f * image.Height;

        // Calculate upper left corner for repositioning the image/boundary.
        var y = (int)(centerY - faceInfo.FaceHeight / _outputConfiguration.FaceHeightRatio / 2);
        var x = (int)(centerX - (centerY - y) * aspectRatio);

        // Determine width and height of the intermediary image based on the actual face height and target face ratio.
        var height = (int)(faceInfo.FaceHeight / _outputConfiguration.FaceHeightRatio);
        var width = (int)(height * aspectRatio);

        var croppingRectangle = new Rectangle(x, y, width, height);

        // Adjust x-position and width incase the original images width is too small.
        AdjustWithAndHeight(image, ref croppingRectangle);

        // Extend height or with to maintain the proper aspect ratio
        FixAspectRatio(ref croppingRectangle, aspectRatio);

        // Extend image if width/height of cropping rectangle exceed the current images dimensions
        ExtendImage(image, ref croppingRectangle);

        // Crop the face and resize the image to the target dimensions
        image.Mutate(img => img.Crop(croppingRectangle)
                               .Resize(_outputConfiguration.Width, _outputConfiguration.Height)
        );

        // Convert the resulting image back to byte array
        using var memoryStream = new MemoryStream();
        image.SaveAsJpeg(memoryStream);
        return memoryStream.ToArray();
    }

    private void AdjustWithAndHeight(Image<Rgba32> image, ref Rectangle rectangle)
    {
        // Adjust x-position and width incase the original images width is too small.
        if (rectangle.Width > image.Width - rectangle.X)
        {
            var deltaX = rectangle.Width - (image.Width - rectangle.X);
            rectangle.Width += deltaX;
            rectangle.X -= deltaX / 2;
            _logger.LogDebug(ValidationLogEvents.AdjustDimensions, "Adjust horizontal dimensions by {deltaX} pixels.", deltaX);
        }

        // Adjust y-position and height incase the original images height is too small.
        if (rectangle.Height > image.Height - rectangle.Y)
        {
            var deltaY = rectangle.Height - (image.Height - rectangle.Y);
            rectangle.Height += deltaY;
            rectangle.Y -= deltaY / 2;
            _logger.LogDebug(ValidationLogEvents.AdjustDimensions, "Adjust vertical dimensions by {deltaY} pixels.", deltaY);
        }
    }

    private void FixAspectRatio(ref Rectangle rectangle, float aspectRatio)
    {
        if ((float)rectangle.Width / rectangle.Height > aspectRatio)
        {
            var newHeight = (int)(rectangle.Width / aspectRatio);
            _logger.LogDebug(ValidationLogEvents.FixAspectRatio,
                             "Fixed the aspect ratio by scaling the height up from {heightBefore} to {newHeight} pixels.",
                             rectangle.Height,
                             newHeight);
            rectangle.Height = newHeight;
        }
        else if ((float)rectangle.Width / rectangle.Height < aspectRatio)
        {
            var newHeight = (int)(rectangle.Height * aspectRatio);
            _logger.LogDebug(ValidationLogEvents.FixAspectRatio,
                             "Fixed the aspect ratio by scaling the height down from {heightBefore} to {newHeight} pixels.",
                             rectangle.Height,
                             newHeight);
            rectangle.Width = newHeight;
        }
    }

    private void ExtendImage(Image<Rgba32> image, ref Rectangle rectangle)
    {
        // Check horizontally
        if (rectangle.X < 0)
        {
            // Extend image on the LEFT side
            var width = image.Width - rectangle.X;
            _logger.LogDebug(ValidationLogEvents.ExtendImage, "Extend the image on the LEFT side by {width} pixels.", width - image.Width);
            image.Mutate(img => img.Resize(new ResizeOptions { Mode = ResizeMode.BoxPad, Position = AnchorPositionMode.Right, Size = new Size(width, image.Height) }));
            rectangle.X = 0;
        }

        if (rectangle.Width + rectangle.X > image.Width)
        {
            // Extend image on the RIGHT side
            var width = rectangle.Width + rectangle.X;
            _logger.LogDebug(ValidationLogEvents.ExtendImage, "Extend the image on the RIGHT side by {width} pixels.", width - image.Width);
            image.Mutate(img => img.Resize(new ResizeOptions { Mode = ResizeMode.BoxPad, Position = AnchorPositionMode.Left, Size = new Size(width, image.Height) }));
        }

        // Check vertically
        if (rectangle.Y < 0)
        {
            // Extend image at the TOP
            var height = image.Height - rectangle.Y;
            _logger.LogDebug(ValidationLogEvents.ExtendImage, "Extend the image on the TOP side by {width} pixels.", height - image.Height);
            image.Mutate(img => img.Resize(new ResizeOptions { Mode = ResizeMode.BoxPad, Position = AnchorPositionMode.Bottom, Size = new Size(image.Width, height) }));
            rectangle.Y = 0;
        }

        if (rectangle.Height + rectangle.Y > image.Height)
        {
            // Extend image at the BOTTOM
            var height = rectangle.Height + rectangle.Y;
            _logger.LogDebug(ValidationLogEvents.ExtendImage, "Extend the image on the BOTTOM side by {width} pixels.", height - image.Height);
            image.Mutate(img => img.Resize(new ResizeOptions { Mode = ResizeMode.BoxPad, Position = AnchorPositionMode.Top, Size = new Size(image.Width, height) }));
        }
    }
}
