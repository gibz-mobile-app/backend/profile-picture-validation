﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using ProfilePictureValidation.Utilities;

namespace ProfilePictureValidation.Services;

public class RemoveBgBackgroundRemovalService(
    IOptions<RemoveBgConfiguration> removeBgConfiguration,
    ILogger<RemoveBgBackgroundRemovalService> _logger) : IBackgroundRemovalService
{
    private readonly RemoveBgConfiguration _removeBgConfiguration = removeBgConfiguration.Value;

    /// <summary>
    /// Removes the background from the given image.
    /// </summary>
    /// <param name="imageFile">Image from which the background should be removed.</param>
    /// <returns>The foreground of the provided image with transparent background.</returns>
    /// <exception cref="InvalidOperationException">Thrown if the background removal process failed.</exception>
    public async Task<byte[]> RemoveBackgroundAsync(byte[] imageFile)
    {
        using var client = GetHttpClient();

        using var formData = new MultipartFormDataContent
        {
            { new ByteArrayContent(imageFile), "image_file", "profilePicture.jpeg" },
            { new StringContent(_removeBgConfiguration.ForegroundType), "type" },
            { new StringContent(_removeBgConfiguration.OutputSize), "size" },
            { new StringContent(_removeBgConfiguration.BackgroundImageUrl), "bg_image_url" }
        };

        var response = await client.PostAsync(_removeBgConfiguration.BackgroundRemovalEndpointPath, formData);

        if (response.IsSuccessStatusCode)
        {
            _ = CheckBalanceAsync();
            return await response.Content.ReadAsByteArrayAsync();
        }
        else
        {
            var responseContent = await response.Content.ReadAsStringAsync();
            _logger.LogError(ValidationLogEvents.BackgroundRemoval,
                             "Failed to remove the background. Status code: {statusCode} (error: {error})",
                             (int)response.StatusCode,
                             responseContent);
            throw new HttpRequestException($"Failed to remove background with HTTP status {(int)response.StatusCode}).");
        }
    }

    private async Task CheckBalanceAsync()
    {
        using var client = GetHttpClient();
        var response = await client.GetAsync(_removeBgConfiguration.AccountInfoEndpointPath);
        if (response.IsSuccessStatusCode)
        {
            try
            {
                string jsonString = await response.Content.ReadAsStringAsync();
                var jsonObject = JsonConvert.DeserializeObject<dynamic>(jsonString);
                int? remainingCredits = jsonObject?.data?.attributes?.credits?.total;

                if (remainingCredits is not null)
                {
                    var severity = remainingCredits <= _removeBgConfiguration.RemainingCreditsThreshold ? LogLevel.Warning : LogLevel.Debug;
                    _logger.Log(severity, ValidationLogEvents.RemainingCreditsReporting, "Remaining credits for remove.bg service: {remainingCredits}", remainingCredits);
                }
            }
            catch (Exception)
            {
                _logger.LogError(ValidationLogEvents.RemainingCreditsReporting, "Error while requesting remaining credits from remove.bg service.");
            }
        }
    }

    private HttpClient GetHttpClient()
    {
        var client = new HttpClient() { BaseAddress = new Uri(removeBgConfiguration.Value.ServiceBaseUrl) };
        client.DefaultRequestHeaders.Add("X-Api-Key", _removeBgConfiguration.ApiKey);

        return client;
    }
}