using System.Net.Http.Headers;
using Microsoft.Extensions.Options;
using ProfilePictureValidation.Exceptions;
using ProfilePictureValidation.Utilities;

namespace ProfilePictureValidation.Services;

public class RemotePicturePersistenceService(
    IOptions<ProfilePictureOutputConfiguration> outputConfiguration,
    IValidationFlagService _validationFlagService,
    ILogger<RemotePicturePersistenceService> _logger
) : IPicturePersistenceService
{
    private readonly ProfilePictureOutputConfiguration _outputConfiguration = outputConfiguration.Value;

    public async Task PersistProfilePictureAsync(string token, byte[] picture, bool doNotUpdateDatabase = false)
    {
        using var formData = new MultipartFormDataContent();
        var uploadImage = new ByteArrayContent(picture);
        uploadImage.Headers.ContentType = MediaTypeHeaderValue.Parse(System.Net.Mime.MediaTypeNames.Image.Png);
        formData.Add(new StringContent(token), "token");
        formData.Add(uploadImage, "profilePicture", "profilePicture.jpeg");

        var flagIndex = 0;
        foreach (var validationFlag in _validationFlagService.GetValidationFlags())
        {
            var fieldName = $"validationFlags[{flagIndex++}]";
            formData.Add(new StringContent(validationFlag.Label), $"{fieldName}[label]");
            formData.Add(new StringContent(validationFlag.Value?.ToString() ?? ""), $"{fieldName}[value]");
        }

        if (doNotUpdateDatabase)
        {
            formData.Add(new StringContent(doNotUpdateDatabase.ToString()), nameof(doNotUpdateDatabase));
        }

        using var httpClient = new HttpClient();
        var response = await httpClient.PostAsync(_outputConfiguration.OutputUrl, formData);

        if (!response.IsSuccessStatusCode)
        {
            var content = await response.Content.ReadAsStringAsync();
            _logger.LogError(ValidationLogEvents.RemotePicturePersistenceFailed, "Persistence of picture failed with status code {statusCode} ({error}).", (int)response.StatusCode, content);
            throw new RemotePicturePersistenceException(response.StatusCode, content);
        }
        else
        {
            _logger.LogDebug(ValidationLogEvents.RemotePicturePersistenceSuccessful, "Successfully persisted picture.");
        }
    }
}