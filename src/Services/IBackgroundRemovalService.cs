﻿namespace ProfilePictureValidation.Services;
public interface IBackgroundRemovalService
{
    Task<byte[]> RemoveBackgroundAsync(byte[] imageFile);
}