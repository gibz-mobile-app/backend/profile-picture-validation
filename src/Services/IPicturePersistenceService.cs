namespace ProfilePictureValidation.Services;

public interface IPicturePersistenceService
{
    public Task PersistProfilePictureAsync(string token, byte[] picture, bool doNotUpdateDatabase = false);
}