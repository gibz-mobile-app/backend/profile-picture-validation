using ProfilePictureValidation.Models;

namespace ProfilePictureValidation.Services;

public interface IValidationFlagService
{
    public void AddValidationFlag(string label, float? value);

    public IEnumerable<ValidationFlag> GetValidationFlags();
}
