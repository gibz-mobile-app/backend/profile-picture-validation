﻿using Google.Cloud.Vision.V1;
using Microsoft.Extensions.Options;
using ProfilePictureValidation.Exceptions;
using ProfilePictureValidation.Models;
using ProfilePictureValidation.Utilities;

namespace ProfilePictureValidation.Services;
public class GoogleFaceValidationService(
    IOptions<ProfilePictureOutputConfiguration> outputConfiguration,
    IOptions<FaceOrientationConfiguration> faceOrientationConfiguration,
    IValidationFlagService _validationFlagService,
    ILogger<GoogleFaceValidationService> _logger
) : IFaceValidationService
{
    private readonly ProfilePictureOutputConfiguration _outputConfiguration = outputConfiguration.Value;
    private readonly FaceOrientationConfiguration _faceOrientationConfiguration = faceOrientationConfiguration.Value;
    private readonly ImageAnnotatorClient _imageAnnotatorClient = ImageAnnotatorClient.Create();

    /// <summary>
    /// Validates the number of faces visible on the image to be 1.
    /// If there's either no or more than one face visible, the validation fails.
    /// </summary>
    /// <param name="imageBytes">Image to be validated for the number of visible faces.</param>
    /// <exception cref="ArgumentException">Thrown if either no or more than one face is visible in the provided image.</exception>
    public async Task<FaceInformation> GetFaceInformationAsync(byte[] imageBytes)
    {
        var image = Image.FromBytes(imageBytes);
        var faces = await _imageAnnotatorClient.DetectFacesAsync(image, maxResults: 2);

        if (faces.Count == 0)
        {
            _logger.LogWarning(ValidationLogEvents.AbnormalFaceCount, "Face detection failed with no detected face.");
            throw new NoFaceDetectedException();
        }
        else if (faces.Count > 1)
        {
            _logger.LogWarning(ValidationLogEvents.AbnormalFaceCount, "Face detection failed with {count} detected faces.", faces.Count);
            throw new MoreThanOneFaceDetectedException();
        }

        var faceInformation = ConvertToFaceInformation(faces[0]);

        _validationFlagService.AddValidationFlag("BlurredLikelihood", (int)faceInformation.BlurredLikelihood);
        _validationFlagService.AddValidationFlag("UnderExposedLikelihood", (int)faceInformation.UnderExposedLikelihood);
        _validationFlagService.AddValidationFlag("HeadWearLikelihood", (int)faceInformation.HeadwearLikelihood);

        return faceInformation;
    }

    /// <summary>
    /// Validate the face orientation.
    /// This method expects exactly 1 face to be contained in the provided image. This could/should be validated prior to calling this method using the ValidateExactlyOneFaceVisible method.
    /// </summary>
    /// <param name="imageBytes">Image containing the face to be validated.</param>
    /// <exception cref="ArgumentException">Thrown if the validation of the face orientation fails.</exception>
    public void ValidateFaceOrientation(FaceInformation faceInformation)
    {
        if (Math.Abs(faceInformation.PanAngle) > _faceOrientationConfiguration.MaxPanAngle)
        {
            _logger.LogWarning(ValidationLogEvents.FaceOrientationOutOfBounds,
                               "Face orientation (pan) out of bounds: {panIs} degrees (allowed: {panAllowed} degrees)",
                               faceInformation.PanAngle,
                               _faceOrientationConfiguration.MaxPanAngle);
            throw new FaceOrientationException("PAN", _faceOrientationConfiguration.MaxPanAngle, faceInformation.PanAngle);
        }

        if (Math.Abs(faceInformation.TiltAngle) > _faceOrientationConfiguration.MaxTiltAngle)
        {
            _logger.LogWarning(ValidationLogEvents.FaceOrientationOutOfBounds,
                               "Face orientation (tilt) out of bounds: {tiltIs} degrees (allowed: {tiltAllowed} degrees)",
                               faceInformation.TiltAngle,
                               _faceOrientationConfiguration.MaxTiltAngle);
            throw new FaceOrientationException("TILT", _faceOrientationConfiguration.MaxPanAngle, faceInformation.PanAngle);
        }
    }

    /// <summary>
    /// Checks whether the face is big enough for uniform cropping the final profile picture.
    /// </summary>
    /// <param name="faceInformation">Meta data describing the faces properties and landmarks.</param>
    /// <exception cref="ArgumentException">Thrown when the face is too small for the resulting profile picture.</exception>
    public void ValidateDimensions(FaceInformation faceInformation)
    {
        if (faceInformation.FaceHeight / (float)_outputConfiguration.Height < _outputConfiguration.FaceHeightRatio)
        {
            _logger.LogWarning(ValidationLogEvents.InsufficientFaceDimensions,
                               "Face is to small for proper cropping (height of face: {faceHeight}, required face height ratio: {requiredRatio})",
                               faceInformation.FaceHeight,
                               _outputConfiguration.FaceHeightRatio);
            throw new FaceTooSmallException();
        }
    }

    private FaceInformation ConvertToFaceInformation(FaceAnnotation face)
    {
        var faceInformation = new FaceInformation
        {
            PanAngle = face.PanAngle,
            RollAngle = face.RollAngle,
            TiltAngle = face.TiltAngle,
            TopOfHead = face.BoundingPoly.Vertices[0].Y,
            BottomOfHead = face.BoundingPoly.Vertices[2].Y,
            UnderExposedLikelihood = GetLikelihood(face.UnderExposedLikelihood),
            BlurredLikelihood = GetLikelihood(face.BlurredLikelihood),
            HeadwearLikelihood = GetLikelihood(face.HeadwearLikelihood)
        };

        var landmarkMapping = new Dictionary<FaceAnnotation.Types.Landmark.Types.Type, EFaceLandmark>
        {
            {FaceAnnotation.Types.Landmark.Types.Type.LeftEye, EFaceLandmark.LEFT_EYE},
            {FaceAnnotation.Types.Landmark.Types.Type.RightEye, EFaceLandmark.RIGHT_EYE},
            {FaceAnnotation.Types.Landmark.Types.Type.NoseTip, EFaceLandmark.NOSE_TIP},
            {FaceAnnotation.Types.Landmark.Types.Type.MidpointBetweenEyes, EFaceLandmark.MIDPOINT_BETWEEN_EYES},
        };

        foreach (var landmarkMap in landmarkMapping)
        {
            var googleLandmark = face.Landmarks.Where(landmark => landmark.Type == landmarkMap.Key).FirstOrDefault();
            if (googleLandmark is null)
            {
                var landmarkLabel = Enum.GetName(typeof(FaceAnnotation.Types.Landmark.Types.Type), landmarkMap.Key);
                _logger.LogWarning(ValidationLogEvents.MissingFaceLandmark, "Missing required face landmark: {landmarkLabel}", landmarkLabel);
                throw new MissingFaceLandmarkException([landmarkLabel]);
            }
            if (googleLandmark is not null)
            {
                var landmark = new FaceLandmark(landmarkMap.Value, googleLandmark.Position.X, googleLandmark.Position.Y);
                faceInformation.Landmarks.Add(landmark);
                _logger.LogDebug(ValidationLogEvents.DetectedFaceLandmark,
                                 "Successfully discovered face landmark: {landmarkLabel} at x={x}, y={y}",
                                 Enum.GetName(typeof(EFaceLandmark), landmark.Type),
                                 googleLandmark.Position.X,
                                 googleLandmark.Position.Y);
            }
        }

        return faceInformation;
    }

    private ELikelihood GetLikelihood(Likelihood likelihood)
    {
        return likelihood switch
        {
            Likelihood.Unknown => ELikelihood.UNKNOWN,
            Likelihood.VeryUnlikely => ELikelihood.VERY_UNLIKELY,
            Likelihood.Unlikely => ELikelihood.UNLIKELY,
            Likelihood.Possible => ELikelihood.POSSIBLE,
            Likelihood.Likely => ELikelihood.LIKELY,
            Likelihood.VeryLikely => ELikelihood.VERY_LIKELY,
            _ => ELikelihood.UNKNOWN
        };
    }
}