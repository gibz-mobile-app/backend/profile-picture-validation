﻿using ProfilePictureValidation.Models;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;

namespace ProfilePictureValidation.Services;
public interface ICroppingService
{
    public Task<byte[]> CropImageAsync(Image<Rgba32> image, FaceInformation faceInformation);
}