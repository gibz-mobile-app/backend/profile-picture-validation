using ProfilePictureValidation.Models;

namespace ProfilePictureValidation.Services;

public class InMemoryValidationFlagService : IValidationFlagService
{
    private readonly List<ValidationFlag> _validationFlags = [];

    public void AddValidationFlag(string label, float? value)
    {
        if (_validationFlags.Any(flag => flag.Label.Equals(label)))
        {
            // Remove existing flags before adding new flag with equal label.
            _validationFlags.RemoveAll(flag => flag.Label.Equals(label));
        }
        _validationFlags.Add(new ValidationFlag(label, value));
    }

    public IEnumerable<ValidationFlag> GetValidationFlags()
    {
        return _validationFlags;
    }
}