namespace ProfilePictureValidation.Exceptions;

public class MoreThanOneFaceDetectedException : ProfilePictureValidationException
{
    public override int ErrorCode => 12;
    public override string Message => "Auf dem eingereichten Profilbild wurden mehrere Gesichter erkannt. Auf dem Profilbild muss genau ein Gesicht sichtbar sein.";
}
