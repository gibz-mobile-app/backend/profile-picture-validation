using System.Net;

namespace ProfilePictureValidation.Exceptions;

public class RemotePicturePersistenceException(HttpStatusCode _statusCode, string? _additionalInformation) : ProfilePictureValidationException
{
    public override int ErrorCode => 31;
    public override string Message => "Das eingereichte Profilbild konnte wegen eines technischen Fehlers nicht gespeichert werden.";
    public override string Details
    {
        get
        {
            var details = $"Status Code: {(int)_statusCode} ({_statusCode})";
            if (!string.IsNullOrWhiteSpace(_additionalInformation))
            {
                details += $" [{_additionalInformation}]";
            }
            return details;
        }
    }
}