namespace ProfilePictureValidation.Exceptions;

public abstract class ProfilePictureValidationException : ApplicationException
{
    public abstract int ErrorCode { get; }
    public virtual string? Details => null;
}