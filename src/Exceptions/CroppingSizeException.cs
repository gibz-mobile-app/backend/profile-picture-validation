namespace ProfilePictureValidation.Exceptions;

public class CroppingSizeException : ProfilePictureValidationException
{
    public override int ErrorCode => 21;
    public override string Message => "Entweder das eingereichte Profilbild oder Ihr Gesicht auf dem Profilbild sind zu klein für den uniformen Zuschnitt des Profilbildes. Stellen Sie sicher, dass Ihr Gesicht die markierte Fläche beim Erstellen des Profilbildes gut ausfüllt.";
}
