namespace ProfilePictureValidation.Exceptions;

public class NoFaceDetectedException : ProfilePictureValidationException
{
    public override int ErrorCode => 11;
    public override string Message => "Auf dem eingereichten Profilbild wurde kein Gesicht erkannt.";
}
