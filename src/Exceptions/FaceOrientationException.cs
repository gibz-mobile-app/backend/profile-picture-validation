namespace ProfilePictureValidation.Exceptions;

public class FaceOrientationException(string _rotation, float _allowedAngle, float _detectedAngle) : ProfilePictureValidationException
{
    public override int ErrorCode => 13;
    public override string Message => "Das Gesicht auf dem Profilbild wurde nicht frontal fotografiert. Stellen Sie sicher, dass der Kopf in keiner Achse zu stark geneigt ist.";
    public override string? Details => $"Rotation: {_rotation} | Erlaubte Rotation: {_allowedAngle} | Erkannte Rotation: {_detectedAngle}";
}
