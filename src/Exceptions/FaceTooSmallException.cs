namespace ProfilePictureValidation.Exceptions;

public class FaceTooSmallException : ProfilePictureValidationException
{
    public override int ErrorCode => 14;
    public override string Message => "Das Gesicht auf dem eingereichten Profilbild ist zu klein. Stellen Sie sicher, dass Ihr Gesicht die markierte Fläche beim Erstellen des Profilbildes gut ausfüllt.";
}
