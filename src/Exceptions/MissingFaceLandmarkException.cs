namespace ProfilePictureValidation.Exceptions;

public class MissingFaceLandmarkException(string?[] _landmarkLabels) : ProfilePictureValidationException
{
    public override int ErrorCode => 15;
    public override string Message => "Auf dem eingereichten Profilbild ist mindestens ein relevanter Teil Ihres Gesichts nicht erkennbar. Stellen Sie sicher, dass Ihr Gesicht bei der Aufnahme nicht verdeckt ist.";
    public override string Details => $"Fehlend: {string.Join(", ", _landmarkLabels)}";
}
